package com.epam.jamp.controller;

import com.epam.jamp.bean.Event;
import com.epam.jamp.exception.EventException;
import com.epam.jamp.exception.detail.EventErrorDetail;
import com.epam.jamp.service.EventService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;

@RestController
@RequestMapping("/event")
@Api(produces = "application/json", tags = {"event-controller"})
@ApiResponses({
        @ApiResponse(code = 200, message = "Success", response = Event.class)
})
public class EventRestController {

    @Autowired
    private EventService eventService;

    @ApiOperation(value = "Create event")
    @PostMapping
    public ResponseEntity<Event> createEvent(@RequestBody Event event) {
        Event createdEvent = eventService.createEvent(event);

        if (createdEvent == null) {
            throw new EventException(EventErrorDetail.EVENT_NOT_CREATED);
        }

        return ResponseEntity.ok(createdEvent);
    }

    @ApiOperation(value = "Get event")
    @GetMapping("/{id}")
    public ResponseEntity<Event> createEvent(@PathVariable BigInteger id) {
        Event createdEvent = eventService.getEventById(id)
                .orElseThrow(() -> new EventException(EventErrorDetail.EVENT_NOT_FOUND));

        return ResponseEntity.ok(createdEvent);
    }

    @ApiOperation(value = "Update event")
    @PutMapping("/{id}")
    public ResponseEntity<Event> updateEvent(@PathVariable BigInteger id, @RequestBody Event event) {
        Event updatedEvent = eventService.updateEvent(id, event);

        if (updatedEvent == null) {
            throw new EventException(EventErrorDetail.EVENT_NOT_UPDATED);
        }

        return ResponseEntity.ok(updatedEvent);
    }

    @ApiOperation(value = "Delete event")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = String.class)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteEvent(@PathVariable BigInteger id) {
        eventService.deleteEvent(id);
        return ResponseEntity.ok().build();
    }

}
