package com.epam.jamp.controller;

import com.epam.jamp.bean.Event;
import com.epam.jamp.bean.Speaker;
import com.epam.jamp.exception.EventException;
import com.epam.jamp.exception.detail.EventErrorDetail;
import com.epam.jamp.service.EventService;
import com.epam.jamp.service.SpeakerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;

@RestController
@RequestMapping("/speaker")
@Api(produces = "application/json", tags = {"speaker-controller"})
@ApiResponses({
        @ApiResponse(code = 200, message = "Success", response = Event.class)
})
public class SpeakerRestController {

    @Autowired
    private SpeakerService speakerService;

    @ApiOperation(value = "Create speaker")
    @PostMapping
    public ResponseEntity<Speaker> createEvent(@RequestBody Speaker speaker) {
        Speaker createdSpeaker = speakerService.createSpeaker(speaker);

        if (createdSpeaker == null) {
            throw new EventException(EventErrorDetail.EVENT_NOT_CREATED);
        }

        return ResponseEntity.ok(createdSpeaker);
    }

    @ApiOperation(value = "Get speaker")
    @GetMapping("/{id}")
    public ResponseEntity<Speaker> createEvent(@PathVariable BigInteger id) {
        Speaker createdSpeaker = speakerService.getSpeakerById(id)
                .orElseThrow(() -> new EventException(EventErrorDetail.EVENT_NOT_FOUND));

        return ResponseEntity.ok(createdSpeaker);
    }

    @ApiOperation(value = "Update speaker")
    @PutMapping("/{id}")
    public ResponseEntity<Speaker> updateEvent(@PathVariable BigInteger id, @RequestBody Speaker speaker) {
        Speaker createdSpeake = speakerService.updateSpeaker(id, speaker);

        if (createdSpeake == null) {
            throw new EventException(EventErrorDetail.EVENT_NOT_UPDATED);
        }

        return ResponseEntity.ok(createdSpeake);
    }

    @ApiOperation(value = "Delete speaker")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = String.class)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteEvent(@PathVariable BigInteger id) {
        speakerService.deleteSpeaker(id);
        return ResponseEntity.ok().build();
    }

}
