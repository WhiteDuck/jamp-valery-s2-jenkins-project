package com.epam.jamp.exception.detail;

public enum EventErrorDetail {

    EVENT_NOT_CREATED("Passed event not created"),
    EVENT_NOT_UPDATED("Event not updated"),
    EVENT_NOT_FOUND("Event not found");

    private String message;

    EventErrorDetail(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
