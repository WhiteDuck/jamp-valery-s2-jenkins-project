package com.epam.jamp.exception;

import com.epam.jamp.exception.detail.EventErrorDetail;

public class EventException extends RuntimeException {

    public EventException(EventErrorDetail eventErrorDetail) {
        super(eventErrorDetail.getMessage());
    }

}
