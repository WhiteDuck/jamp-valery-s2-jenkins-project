package com.epam.jamp.type;

public enum EventType {

    WORKSHOP,
    TECH_TALKS,
    TRAINING,
    MEETING,
    PRAISE

}
