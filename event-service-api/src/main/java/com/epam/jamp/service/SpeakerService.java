package com.epam.jamp.service;

import com.epam.jamp.bean.Speaker;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface SpeakerService {

	/***
	 * Create Speaker by passed Speaker
	 *
	 * @param speaker
	 * @return
	 */
    Speaker createSpeaker(Speaker speaker);

	/***
	 * Update existing Speaker by Speaker and id
	 *
	 * @param id
	 * @param speaker
	 * @return
	 */
    Speaker updateSpeaker(BigInteger id, Speaker speaker);

	/***
	 * Get existing Speaker by passed id
	 *
	 * @param id
	 * @return
	 */
    Optional<Speaker> getSpeakerById(BigInteger id);

	/***
	 * Delete existing Speaker by passed id
	 *
	 * @param id
	 */
	void deleteSpeaker(BigInteger id);

	/***
	 * Get list of existing Speakers
	 *
	 * @return
	 */
    List<Speaker> getAllSpeakers();

}
