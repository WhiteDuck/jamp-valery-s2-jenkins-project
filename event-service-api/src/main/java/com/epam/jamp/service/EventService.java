package com.epam.jamp.service;

import com.epam.jamp.bean.Event;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface EventService {

	/***
	 * Create event by passed event
	 *
	 * @param event
	 * @return
	 */
    Event createEvent(Event event);

	/***
	 * Update existing event by event and id
	 *
	 * @param id
	 * @param event
	 * @return
	 */
    Event updateEvent(BigInteger id, Event event);

	/***
	 * Get existing event by passed id
	 *
	 * @param id
	 * @return
	 */
    Optional<Event> getEventById(BigInteger id);

	/***
	 * Delete existing event by passed id
	 *
	 * @param id
	 */
	void deleteEvent(BigInteger id);

	/***
	 * Get list of existing events
	 *
	 * @return
	 */
    List<Event> getAllEvents();

	/***
	 * Get list of existing events by title
	 *
	 * @param title
	 * @return
	 */
    List<Event> getAllEventsByTitle(String title);

}
