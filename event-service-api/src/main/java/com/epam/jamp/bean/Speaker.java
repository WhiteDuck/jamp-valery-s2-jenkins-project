package com.epam.jamp.bean;

import org.springframework.data.annotation.Id;

import java.math.BigInteger;

public class Speaker {

    @Id
    private BigInteger id;
    private String firstName;
    private String lastName;
    private Email email;

    public Speaker() {
    }

    public Speaker(BigInteger id, String firstName, String lastName, Email email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

}
