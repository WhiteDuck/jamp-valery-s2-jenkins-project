package com.epam.jamp.bean;

import com.epam.jamp.type.EventType;
import org.springframework.data.annotation.Id;

import java.math.BigInteger;
import java.time.LocalDateTime;

public class Event {

    @Id
    private BigInteger id;
    private String title;
    private Address address;
    private Speaker speaker;
    private EventType eventType;
    private LocalDateTime dateTime;

    public Event() {
    }

    public Event(BigInteger id, String title, Address address, Speaker speaker, EventType eventType, LocalDateTime dateTime) {
        this.id = id;
        this.title = title;
        this.address = address;
        this.speaker = speaker;
        this.eventType = eventType;
        this.dateTime = dateTime;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Speaker getSpeaker() {
        return speaker;
    }

    public void setSpeaker(Speaker speaker) {
        this.speaker = speaker;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

}
