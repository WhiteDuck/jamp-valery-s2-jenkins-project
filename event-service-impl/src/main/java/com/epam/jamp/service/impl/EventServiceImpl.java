package com.epam.jamp.service.impl;

import com.epam.jamp.bean.Event;
import com.epam.jamp.service.EventService;
import com.epam.jamp.service.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventRepository eventRepository;

    @Override
    public Event createEvent(Event event) {
        return eventRepository.save(event);
    }

    @Override
    public Event updateEvent(BigInteger id, Event event) {
        Optional<Event> exitedEvent = this.getEventById(id);

        Event updatedEvent = null;
        if (exitedEvent.isPresent()) {
            event.setId(id);
            updatedEvent = eventRepository.save(event);
        }

        return updatedEvent;
    }

    @Override
    public Optional<Event> getEventById(BigInteger id) {
        return eventRepository.findById(id);
    }

    @Override
    public void deleteEvent(BigInteger id) {
        eventRepository.deleteById(id);
    }

    @Override
    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    @Override
    public List<Event> getAllEventsByTitle(String title) {
        return eventRepository.findAllByTitle(title);
    }

}
