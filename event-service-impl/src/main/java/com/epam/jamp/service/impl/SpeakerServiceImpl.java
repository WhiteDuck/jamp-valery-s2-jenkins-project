package com.epam.jamp.service.impl;

import com.epam.jamp.bean.Speaker;
import com.epam.jamp.service.SpeakerService;
import com.epam.jamp.service.repository.SpeakerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Service
public class SpeakerServiceImpl implements SpeakerService {

    @Autowired
    private SpeakerRepository speakerRepository;

    @Override
    public Speaker createSpeaker(Speaker speaker) {
        return speakerRepository.save(speaker);
    }

    @Override
    public Speaker updateSpeaker(BigInteger id, Speaker speaker) {
        Optional<Speaker> exitedEvent = this.getSpeakerById(id);

        Speaker updatedSpeaker = null;
        if (exitedEvent.isPresent()) {
            updatedSpeaker.setId(id);
            updatedSpeaker = speakerRepository.save(speaker);
        }

        return updatedSpeaker;
    }

    @Override
    public Optional<Speaker> getSpeakerById(BigInteger id) {
        return speakerRepository.findById(id);
    }

    @Override
    public void deleteSpeaker(BigInteger id) {
        speakerRepository.deleteById(id);
    }

    @Override
    public List<Speaker> getAllSpeakers() {
        return speakerRepository.findAll();
    }

}
