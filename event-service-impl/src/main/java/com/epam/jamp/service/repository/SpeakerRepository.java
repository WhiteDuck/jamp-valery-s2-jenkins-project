package com.epam.jamp.service.repository;

import com.epam.jamp.bean.Speaker;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface SpeakerRepository extends MongoRepository<Speaker, BigInteger> {
}
