package com.epam.jamp.service.repository;

import com.epam.jamp.bean.Event;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface EventRepository extends CrudRepository<Event, BigInteger> {

    /**
     * Get all entities as List
     *
     * @return
     */
    List<Event> findAll();

    /**
     * Get all entities by title as List
     *
     * @return
     */
    List<Event> findAllByTitle(String title);

}
